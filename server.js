const express = require('express');
const users = require('./src/users/userRoutes');
// const checklists = require('./src/checklist/routesCheck');

// const{ success } = require('./response')

const app = express();
const port = 3000;

app.use(express.json());

app.use("/users", users);
// app.use("/checklist" , checklists);

app.listen(port, () => {
    console.log(`Example app listening on port http://localhost:${port}`)
})

app.get('/', (req, res) => {
    res.send('Hello World!')
})

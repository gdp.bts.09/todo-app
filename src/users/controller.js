const pool = require('../../database');
const queries = require('./queries');
const bcrypt = require('bcryptjs');

//login
const getUsersByEmail = async (req, res) => {
  const body = req.body;
  const user = await pool.query(queries.getUsersByEmail, [body.user_email]);
  if (user) {
    // check user password with hashed password stored in the database
    console.log(body.password, user.rows[0].password)
    const validPassword = await bcrypt.compare(body.password, user.rows[0].password);
    if (validPassword) {
      res.status(200).json({ message: "Valid password" });
    } else {
      res.status(400).json({ error: "Invalid Password" });
    }
  } else {
    res.status(401).json({ error: "User does not exist" });
  }
}

//register
const registerUser = async (req, res) => {
  const body = req.body;

  if (!(body.user_email && body.password)) {
    res.status(400).send({ error: "Data not formatted properly" });
  }

  const salt = await bcrypt.genSalt(10);

  const passwordCrypt = await bcrypt.hash(body.password, salt);
  await pool.query(queries.registerUser, [body.user_id, body.user_name, body.user_email, passwordCrypt]);
  res.status(200).json({ message: "success" });
}

module.exports = {
  getUsersByEmail,
  registerUser
}
